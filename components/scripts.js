//Include google analytics if present
function googleAnalytics(data){
  if(data.acf.google_analytics_code) {
    return `
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', '${data.acf.google_analytics_code}');
    `
  } else {
    return '';
  }
}

//nav link scroll to calculator
function scrollToCalc(){
  return `
(function($) {
  $(function(){
    $('.scrolllink').click(function(e) {
      e.preventDefault();
      $('html').animate({
        scrollTop: $("#Calculator").offset().top
      })
    });
  })
})(jQuery);
`
}

//nav link scroll to calculator
function phoneClickTracking(data){
  if(data.acf.phone_number_ppc_code) {
    return `
(function($) {
  $(function(){
    $('.phone-click-track').click(function(e) {
      try{
         __adroll.record_user({"adroll_segments": "${data.acf.phone_number_ppc_code}"})
      } catch(err) {}
    });
  })
})(jQuery);
`
  } else {
    return '';
  }
}

function renderScripts(data) {
  let scripts = `
    ${googleAnalytics(data)}
    ${phoneClickTracking(data)}
    ${scrollToCalc()}
  `;

  return scripts;
}

module.exports = {
  renderScripts
}
