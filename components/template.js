function getHeroImage(data) {
  if(data.hero.image) {
    return `background-image: url('${data.hero.image}'); background-size: cover;`;
  } else {
    return `background-image: url('https://rmlending.com/wp-content/themes/RML/img/home-calc-hero.jpg');` ;
  }
}

function renderTemplate(data) {
  let acf = data.acf;
  let html = `
  <!DOCTYPE html>
     <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-mobile-web-app-title" content="Reverse Mortgage Lending - America&#039;s Premier Reverse Mortgage Lender">
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="https://rmlending.com/xmlrpc.php">

      <link rel="apple-touch-icon" sizes="57x57" href="https://rmlending.com/favicon/apple-icon-57x57.png">
      <link rel="apple-touch-icon" sizes="60x60" href="https://rmlending.com/favicon/apple-icon-60x60.png">
      <link rel="apple-touch-icon" sizes="72x72" href="https://rmlending.com/favicon/apple-icon-72x72.png">
      <link rel="apple-touch-icon" sizes="76x76" href="https://rmlending.com/favicon/apple-icon-76x76.png">
      <link rel="apple-touch-icon" sizes="114x114" href="https://rmlending.com/favicon/apple-icon-114x114.png">
      <link rel="apple-touch-icon" sizes="120x120" href="https://rmlending.com/favicon/apple-icon-120x120.png">
      <link rel="apple-touch-icon" sizes="144x144" href="https://rmlending.com/favicon/apple-icon-144x144.png">
      <link rel="apple-touch-icon" sizes="152x152" href="https://rmlending.com/favicon/apple-icon-152x152.png">
      <link rel="apple-touch-icon" sizes="180x180" href="https://rmlending.com/favicon/apple-icon-180x180.png">
      <link rel="icon" type="image/png" sizes="192x192"  href="https://rmlending.com/android-icon-192x192.png">
      <link rel="icon" type="image/png" sizes="32x32" href="https://rmlending.com/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="96x96" href="https://rmlending.com/favicon/favicon-96x96.png">
      <link rel="icon" type="image/png" sizes="16x16" href="https://rmlending.com/favicon/favicon-16x16.png">
      <link rel="manifest" href="https://rmlending.com/favicon/manifest.json">

      <link rel="stylesheet" href="style.css">

      <script src="https://rmlending.com/wp-includes/js/jquery/jquery.js?ver=1.12.4" ></script>
      <script src="https://rmlending.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1" ></script>
      <script src="main.js" ></script>
      <script src="https://rmlending.com/wp-content/themes/RML/js/rangeslider.min.js?ver=4.9.8" ></script>
      <script src="https://rmlending.com/wp-content/themes/RML/js/slick.min.js?ver=4.9.8" ></script>
      <script src="https://rmlending.com/wp-content/themes/RML/js/calc.js?ver=4.9.8" ></script>
      <title>${acf.logo.title} ${acf.logo.subtitle}</title>
     </head>
     <body class="home page-template page-template-page-templates page-template-home-calc page-template-page-templateshome-calc-php page page-id-300">

        <div class="hfeed site" id="page">
           <!-- ******************* The Navbar Area ******************* -->
           <div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar">
              <a class="skip-link screen-reader-text sr-only" href="#content">Skip to content</a>
              <nav class="navbar navbar-toggleable-md">
                 <div class="container">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <!-- Your site title as branding in the menu -->
                    <div class="logo_header">
                       <img src="./assets/logo.svg" alt="Reverse Mortgage Lending Logo"/>
                       <div class="logo-text">
                       <h3>${acf.logo.title}</h3>
                       <p>${acf.logo.subtitle}</p>
                       </div>
                    </div>
                    <h1 class="logo-name">Reverse Mortgage Lending</h1>
                    <!-- The WordPress Menu goes here -->

                    <nav class="right">
                      <p class="phone-wrapper">Call Now: <a href="tel:${acf.phone}" class="h3 blue phone phone-click-track">${acf.phone}</a></p>
                    <ul>
                      <li><a href="#Calculator" class="scrolllink">Reverse Mortgage Calculator</a></li>

                      <li><a href="tel:${acf.phone}" class="phone-click-track">Contact</a></li>
                    </ul>
                  </nav>
                 </div>
                 <!-- .container -->
              </nav>
              <!-- .site-navigation -->
           </div>
           <!-- .wrapper-navbar end -->
           <div class="landing-page-wrapper">
              <section class="city-banner" id="Home">
                 <span class="bg home-bg" style="${getHeroImage(acf)}" role="img" aria-label=""></span>
                 <div class="container">
                    <div class="form-area">
                       <div class="flex-area" id="Calculator">
                          <div class="content">
                             <h1>${acf.hero.text}</h1>
                          </div>
                          <article class="form">
                             <h2>${acf.hero.calc_title}</h2>
                             <h3 class="sub">${acf.hero.calc_copy}</h3>
                             <form class="calc-form">
                                <div class="form-section age">
                                   <label for="age">Age <span>of youngest borrower</span></label>
                                   <select class="form-age" name="form-age">
                                      <option value="62">62</option>
                                      <option value="63">63</option>
                                      <option value="64">64</option>
                                      <option value="65">65</option>
                                      <option value="66">66</option>
                                      <option value="67">67</option>
                                      <option value="68">68</option>
                                      <option value="69">69</option>
                                      <option value="70">70</option>
                                      <option value="71">71</option>
                                      <option value="72">72</option>
                                      <option value="73">73</option>
                                      <option value="74">74</option>
                                      <option value="75">75</option>
                                      <option value="76">76</option>
                                      <option value="77">77</option>
                                      <option value="78">78</option>
                                      <option value="79">79</option>
                                      <option value="80">80</option>
                                      <option value="81">81</option>
                                      <option value="82">82</option>
                                      <option value="83">83</option>
                                      <option value="84">84</option>
                                      <option value="85">85</option>
                                      <option value="86">86</option>
                                      <option value="87">87</option>
                                      <option value="88">88</option>
                                      <option value="89">89</option>
                                      <option value="90">90</option>
                                      <option value="91">91</option>
                                      <option value="92">92</option>
                                      <option value="93">93</option>
                                      <option value="94">94</option>
                                      <option value="95">95</option>
                                      <option value="96">96</option>
                                      <option value="97">97</option>
                                      <option value="98">98</option>
                                      <option value="99">99</option>
                                   </select>
                                </div>
                                <div class="form-section slider-section">
                                   <div class="upper">
                                      <label for="age">Mortgage Balance</label>
                                      <!-- <h4 class="loan-value-output"></h4> -->
                                      <span>$ <input type="text" name="loan-value-output" class="loan-value-output pretty-input" value=""></span>
                                   </div>
                                   <input class="form-loan-value" type="range" min="0" max="6000000" step="1" value="75000" >
                                   <div class="lower">
                                      <div class="left">
                                         <p>$0</p>
                                      </div>
                                      <div class="right">
                                         <p>$6,000,000</p>
                                      </div>
                                   </div>
                                </div>
                                <div class="form-section slider-section">
                                   <div class="upper">
                                      <label for="age">Home Value</label>
                                      <!-- <h4 class="home-value-output"></h4> -->
                                      <span>$ <input type="text" name="loan-value-output" class="home-value-output pretty-input" value=""></span>
                                   </div>
                                   <input class="form-home-value" type="range" min="0" max="10000000" step="1" value="310000" >
                                   <div class="lower">
                                      <div class="left">
                                         <p>$0</p>
                                      </div>
                                      <div class="right">
                                         <p>$10,000,000</p>
                                      </div>
                                   </div>
                                </div>
                                <!-- <div class="form-section available-now-box">
                                   <label for="available-now">Available Funds</label>
                                   <h3 class="blue available-now h1"></h3>
                                   </div> -->
                             </form>
                             <div class="prequalify">
                                <h4 class="blue h3">Pre-Qualify for this amount today</h4>
                                <h3 class="h1 light prequalify-amount">$352,000</h3>
                                <p class="grey">Available Now</p>
                                <a href="tel:${acf.phone}" class="btn_6 phone-click-track">prequalify in 90 seconds</a>
                                <p>Pre-qualifying will not affect your credit score</p>
                             </div>
                       </div>
                       </article>
                    </div>
                 </div>
              </section>
              <section class="blue-banner below-hero">
                 <div class="container">
                    <article class="main-content">
                       <h2 class="blue">${acf.hero.below_hero_title}</h2>
                       <p class="grey">${acf.hero.below_hero_copy}</p>
                       <a href="tel:${acf.phone}" class="h3 below-hero-phone phone-click-track">${acf.phone}</a>
                    </article>
                 </div>
              </section>
              <div class="container">
                 <div class="keep-simple-area" id="scroll_1">
                    <h2 class="h2">
                    It’s Easy To Get The Money You Need</h1>
                    <p class="narrow_w">Pre-qualify with just a few clicks. Fill out the form, find out how much you may receive and get it fast.</p>
                    <div class="row">
                       <div class="col-md-4">
                          <div class="circ qualify">
                             <img src="https://rmlending.com/wp-content/themes/RML/img/brand_icons/qualify.svg" alt="Qualify">
                          </div>
                          <h4 class="h4">PRE-Qualify</h4>
                       </div>
                       <div class="col-md-4">
                          <div class="circ approval">
                             <img src="https://rmlending.com/wp-content/themes/RML/img/brand_icons/approval.svg" alt="Approval">
                          </div>
                          <h4 class="h4">Approval</h4>
                       </div>
                       <div class="col-md-4">
                          <div class="circ fund">
                             <img src="https://rmlending.com/wp-content/themes/RML/img/brand_icons/fund.svg" alt="Fund">
                          </div>
                          <h4 class="h4">Funding</h4>
                       </div>
                    </div>
                 </div>
                 <hr>
                 <div class="rm-options-area">
                    <h2 class="h2">A reverse Mortgage will help you:</h2>
                    <div class="row">
                       <div class="col-lg-4 col-md-6">
                          <h4 class="h4">Protect Your Future</h4>
                          <hr class="micro">
                          <p>Improve your financial security and have money for the things you need.</p>
                       </div>
                       <div class="col-lg-4 col-md-6">
                          <h4 class="h4">Live Healthy</h4>
                          <hr class="micro">
                          <p>Pay off healthcare costs and support your active lifestyle.</p>
                       </div>
                       <div class="col-lg-4 col-md-6">
                          <h4 class="h4">PAY EXPENSES</h4>
                          <hr class="micro">
                          <p>Stop making principal and interest payments</p>
                       </div>
                       <div class="col-lg-4 col-md-6">
                          <h4 class="h4">Have fun</h4>
                          <hr class="micro">
                          <p>Travel, relax and live the life you deserve.</p>
                       </div>
                       <div class="col-lg-4 col-md-6">
                          <h4 class="h4">Help others</h4>
                          <hr class="micro">
                          <p>Help family or friends who need a hand.</p>
                       </div>
                       <div class="col-lg-4 col-md-6">
                          <h4 class="h4">New horizons</h4>
                          <hr class="micro">
                          <p>Whatever your dreams, our personalized benefits can get you there.</p>
                       </div>
                    </div>
                 </div>
              </div>
              <!-- container -->
              <div class="home-cta-banner">
                 <div class="container">
                    <h2 class="h2 ">${acf.cta_text}</h2>
                    <a href="tel:${acf.phone}" class="btn_2 phone-click-track">Call Now</a>
                 </div>
              </div>
              <div class="we-provide-area">
                 <div class="containerx">
                    <div class="row">
                       <div class="col-md-6 provide-left">
                          <div class="quote">
                             <p>Individual quotes. Dedicated service. Low and no fee loans. What more do you need?</p>
                             <!-- <div class="trusted-person">Trusted Person</div> -->
                          </div>
                       </div>
                       <div class="col-md-6 provide-right">
                          <div class="provide">
                             <h2 class="h2">We offer</h2>
                             <hr class="micro">
                             <h3 class="h3">A free, no obligation quote</h3>
                             <p>After completing the form to qualify, you will receive a personalized quote to make an informed decision.</p>
                             <hr class="micro">
                             <h3 class="h3">Expert Service</h3>
                             <p>Customer service professionals are standing by to answer all your questions and carefully explain the process.</p>
                             <hr class="micro">
                             <h3 class="h3">Sound Advice</h3>
                             <p>Our reverse mortgage specialists will go over the loan terms in plain language and provide you sound advice.</p>
                             <hr class="micro">
                             <h3 class="h3">Zero fee loans</h3>
                             <p>Zero fee and low fee loans are available for those who qualify.</p>
                          </div>
                       </div>
                    </div>
                    <!--row-->
                 </div>
              </div>
           </div>
           <footer class="site-footer">

              <div class="landing-page-wrapper">
                 <section class="landing-banner footer-section" style="margin-top:0;">
                    <div class="container">
                       <div class="form-area">
                          <div class="flex-area">
                             <article class="form calc-2">
                                <h2>${acf.above_footer.calc_title}</h2>
                                <h3>${acf.above_footer.calc_copy}</h3>
                                <form class="calc-form">
                                   <div class="form-section age">
                                      <label for="age">Age <span>of youngest borrower</span></label>
                                      <select class="form-age" name="form-age">
                                         <option value="62">62</option>
                                         <option value="63">63</option>
                                         <option value="64">64</option>
                                         <option value="65">65</option>
                                         <option value="66">66</option>
                                         <option value="67">67</option>
                                         <option value="68">68</option>
                                         <option value="69">69</option>
                                         <option value="70">70</option>
                                         <option value="71">71</option>
                                         <option value="72">72</option>
                                         <option value="73">73</option>
                                         <option value="74">74</option>
                                         <option value="75">75</option>
                                         <option value="76">76</option>
                                         <option value="77">77</option>
                                         <option value="78">78</option>
                                         <option value="79">79</option>
                                         <option value="80">80</option>
                                         <option value="81">81</option>
                                         <option value="82">82</option>
                                         <option value="83">83</option>
                                         <option value="84">84</option>
                                         <option value="85">85</option>
                                         <option value="86">86</option>
                                         <option value="87">87</option>
                                         <option value="88">88</option>
                                         <option value="89">89</option>
                                         <option value="90">90</option>
                                         <option value="91">91</option>
                                         <option value="92">92</option>
                                         <option value="93">93</option>
                                         <option value="94">94</option>
                                         <option value="95">95</option>
                                         <option value="96">96</option>
                                         <option value="97">97</option>
                                         <option value="98">98</option>
                                         <option value="99">99</option>
                                      </select>
                                   </div>
                                   <div class="form-section slider-section">
                                      <div class="upper">
                                         <label for="age">Mortgage Balance</label>
                                         <!-- <h4 class="loan-value-output"></h4> -->
                                         <span>$ <input type="text" name="loan-value-output" class="loan-value-output pretty-input" value=""></span>
                                      </div>
                                      <input class="form-loan-value" type="range" min="0" max="6000000" step="1" value="75000" >
                                      <div class="lower">
                                         <div class="left">
                                            <p>$0</p>
                                         </div>
                                         <div class="right">
                                            <p>$6,000,000</p>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="form-section slider-section">
                                      <div class="upper">
                                         <label for="age">Home Value</label>
                                         <!-- <h4 class="home-value-output"></h4> -->
                                         <span>$ <input type="text" name="loan-value-output" class="home-value-output pretty-input" value=""></span>
                                      </div>
                                      <input class="form-home-value" type="range" min="0" max="10000000" step="1" value="310000" >
                                      <div class="lower">
                                         <div class="left">
                                            <p>$0</p>
                                         </div>
                                         <div class="right">
                                            <p>$10,000,000</p>
                                         </div>
                                      </div>
                                   </div>
                                   <!-- <div class="form-section available-now-box">
                                      <label for="available-now">Available Funds</label>
                                      <h3 class="blue available-now h1"></h3>
                                      </div> -->
                                </form>
                             </article>
                             <span></span>
                          </div>
                       </div>
                    </div>
                 </section>
                 <section class="pre-qualify footer-section">
                    <h2 class="h3 blue">Pre-Qualify today</h2>
                    <h3 class="h1 light prequalify-amount">$352,000</h3>
                    <a href="tel:${acf.phone}" class="btn_6 phone-click-track">prequalify in 90 seconds</a>
                    <p>Pre-qualifying will not affect your credit score</p>
                 </section>
              </div>

              <div class="bottom-footer">
                 <div class="container">
                    <div class="upper">
                       <div>
                          <div class="ehl-logo"><img src="https://rmlending.com/wp-content/themes/RML/img/logo_footer_2.svg" alt="Equal Housing Lender Logo"></div>
                          <span>Copyright &copy; ${(new Date()).getFullYear()} Reverse Mortgage Lending, Inc. All Rights Reserved.<br>
                          Reverse Mortgage Lending, Inc. ${acf.footer.address}<br>
                          NMLS #${acf.footer.nmls}. <a href="http://www.nmlsconsumeraccess.org/EntityDetails.aspx/COMPANY/1436354" target="_blank">www.nmlsconsumeraccess.org</a> | <a href="https://rmlending.com/privacy-policy">Privacy Policy</a> | <a href="https://rmlending.com/licensing">Licensing</a></span>
                       </div>
                       <a href="https://www.bbb.org/us/ca/san-diego/profile/reverse-mortgage/reverse-mortgage-lending-inc-1126-172006653" target="_blank" rel="nofollow">
                       <img class="bbb-logo" src="https://rmlending.com/wp-content/themes/RML/img/RML-BBB.svg" alt="Better Business Bureau" />
                       </a>
                    </div>
                    <p style="font-size: 1.2rem; line-height: 2.2rem; margin-top: 50px;">${acf.footer.copywrite}</p>
                 </div>
              </div>
           </footer>
        </div>
     </body>
  </html>
  `;

  return html;
}

module.exports = {
  renderTemplate
}
