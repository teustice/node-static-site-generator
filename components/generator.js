const fs = require('fs-extra');
const fetch = require("node-fetch");
const { exec } = require('child_process');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const template = require('./template');
const style = require('./style');
const scripts = require('./scripts');
const robots = require('./robots');

let PRODUCTION = true;

if(process.env.SET_DEV) {
  PRODUCTION = false;
}

//fetch data
if(!PRODUCTION) {
  generateSplash();
}

function generateSplash() {

  console.log(' - - - - > requesting from API');

  let url;

  if(PRODUCTION) {
    url = 'https://YOUR_WORDPRESS_INSTALL.com/wp-json/wp/v2/splashpages/?per_page=500';
  } else {
    url = 'https://YOUR_WORDPRESS_INSTALL.com/wp-json/wp/v2/splashpages/?per_page=5';
  }

  console.log(url);


  fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(json) {
    console.log(' - - - - > Successfully requested from API');
    console.log('TOTAL SITES: ' + json.length);

    generateHtml(json)
  });

}


//generate HTML | TAKES API DATA PARAM
function generateHtml(data) {
  let sites = [];

  //loop through each site and render content
  for (var i = 0; i < data.length; i++) {
    sites.push({
      title: entities.decode(data[i].title.rendered),
      domain: data[i].acf.domain,
      template: template.renderTemplate(data[i]),
      css: style.renderStyle({
        h1Color: undefined
      }),
      js: scripts.renderScripts(data[i])
    });
  }

  console.log(' - - - - >Injecting data into templates');


  renderWebsites(sites);
}


//takes an array of website data
function renderWebsites(sites) {
  //Loop through website data and generate website
  for (var i = 0; i < sites.length; i++) {
    let dir;

    //set website directory
    if(PRODUCTION) {
      dir = `/var/www/${sites[i].title}`;
    } else {
      dir = `./output/${sites[i].title}`;
    }

    //save HTML
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    fs.writeFile(`${dir}/index.html`, sites[i].template, (err) => {
      // throws an error, you could also catch it here
      if (err) throw err;

      // success case, the file was saved
    });

    //save CSS
    fs.writeFile(`${dir}/style.css`, sites[i].css, (err) => {
      // throws an error, you could also catch it here
      if (err) throw err;

      // success case, the file was saved
    });

    //save JS
    fs.writeFile(`${dir}/main.js`, sites[i].js, (err) => {
      // throws an error, you could also catch it here
      if (err) throw err;

      // success case, the file was saved
    });

    //save robots.txt
    fs.writeFile(`${dir}/robots.txt`, robots.renderRobots(), (err) => {
      // throws an error, you could also catch it here
      if (err) throw err;

      // success case, the file was saved
    });

    //copy assets
    if(!fs.existsSync(`${dir}/assets`)) {
      fs.copy('assets', `${dir}/assets`, err =>{
        if(err) return console.error(err);
      });
    }


    //generate apache configuration
    let domain;
    if(sites[i].domain) {
      domain = sites[i].domain;
    }

    if(domain && PRODUCTION) {
      let conf = `
<VirtualHost *:80>
  ServerAdmin tanner@cheshirebeane.com
  ServerName ${domain}
  ServerAlias www.${domain}
  DocumentRoot "/var/www/${sites[i].title}"
  RewriteEngine on
  RewriteCond %{SERVER_NAME} =${domain} [OR]
  RewriteCond %{SERVER_NAME} =www.${domain}
  RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>`

      fs.writeFile(`/etc/apache2/sites-enabled/${domain}.conf`, conf, err =>{
        if(err) return console.error(err);
      });

      // exec(`sudo a2ensite ${domain}.conf`);
    }
  }

  console.log(' - - - - > successfully generated websites!');
}


module.exports = {
  generateSplash
}
