var express = require("express");
const generator = require('./components/generator');

var app = express();

app.post("/generate-splash-pages", (req, res, next) => {
  generator.generateSplash();
  res.json('Generated');

});

app.listen(3000, () => {
 console.log("Server running on port 3000");
});
